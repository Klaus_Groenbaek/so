package com.example.validation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EnumTest {

    @LocalServerPort
    private int port;

    @Test
    public void testAddAllowed() {

        String invalid =
                "{" +
                        "\"someEnum\" : \"Invalid Value\"" +
                "}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<Application.SomeDTO> exchange = new RestTemplate().postForEntity( "http://localhost:" + port + "/",  new HttpEntity<String>(invalid, headers), Application.SomeDTO.class);


    }


}
